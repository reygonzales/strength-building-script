#!/usr/bin/env xcrun swift

import Cocoa

func orm(weight: Int, reps: Int) -> Int {
    return (Int)((Float)(weight) / (1.0278 - ((Float)(reps) * 0.0278)))
}

func estimatedWeight(reps: Int, orm: Int) -> Int {
    let coefficients = [1.00, 0.943, 0.906, 0.881, 0.856, 0.831, 0.807, 0.786, 0.765, 0.744]
    
    if reps < 1 || reps > 10 {
        return 0
    }
    
    return (Int)(coefficients[reps - 1] * (Double)(orm))
}

func suggestedReps(oneRepMax: Int) -> Array<(weight: Int, reps: Int)> {
    var possibleSets:[(weight: Int, reps: Int)] = []
    let reps:[Int] = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10]
    let lowestIncrement = 5

    for weightDifference in -60...0 {
        if (weightDifference + oneRepMax) % lowestIncrement != 0 {
            continue
        }
        for repCount in reps {
            let weight = oneRepMax + weightDifference
            let ormValue = orm(weight, repCount)
            if ormValue - oneRepMax < 10 && ormValue >= oneRepMax {
                possibleSets.append(weight: weight, reps: repCount)
            }
        }
    }
    
    possibleSets = sorted(possibleSets) { orm($0.weight, $0.reps) < orm($1.weight, $1.reps) }
    
    return possibleSets
}

let lastWeight = Process.arguments[1].toInt()!
let lastReps = Process.arguments[2].toInt()!
let oneRepMax = orm(lastWeight, lastReps)

println("--------------------")
println("One Rep Max")
println("--------------------")
println(oneRepMax)

println("\n--------------------")
println("Estimated reps")
println("--------------------")
let reps:[Int] = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10]
for repCount in reps {
    println("\(estimatedWeight(repCount, oneRepMax))x\(repCount)")
}

println("\n--------------------")
println("Next set suggestions")
println("--------------------")
let calculatedReps = suggestedReps(oneRepMax)
for tuple in calculatedReps {
    let suggestion = tuple as (weight: Int, reps: Int)
    if suggestion.weight == lastWeight && suggestion.reps == lastReps {
        continue
    }
    println("\(suggestion.weight)x\(suggestion.reps), ORM: \(orm(suggestion.weight, suggestion.reps))")
}

println("\n--------------------")
println("12-14 Week Periodization")
println("--------------------")
println("Week 1 - \((Int)(0.7 * (Float)(oneRepMax)))x10 for 2 sets")
println("Week 2 - \((Int)(0.7 * (Float)(oneRepMax)))x10 for 2 sets")
println("Week 3 - \((Int)(0.73 * (Float)(oneRepMax)))x8 for 2 sets")
println("Week 4 - \((Int)(0.76 * (Float)(oneRepMax)))x8 for 2 sets")
println("Week 5 - \((Int)(0.79 * (Float)(oneRepMax)))x5 for 2 sets")
println("Week 6 - \((Int)(0.82 * (Float)(oneRepMax)))x5 for 2 sets")
println("Week 7 - \((Int)(0.85 * (Float)(oneRepMax)))x5 for 2 sets")
println("Week 8 - \((Int)(0.88 * (Float)(oneRepMax)))x5 for 2 sets")
println("Week 9 - \((Int)(0.91 * (Float)(oneRepMax)))x3 for 2 sets")
println("Week 10 - \((Int)(0.94 * (Float)(oneRepMax)))x3 for 2 sets")
println("Week 11 - \((Int)(0.97 * (Float)(oneRepMax)))x2 for 2 sets")
println("Week 12 - \((Int)(1.0 * (Float)(oneRepMax)))x2 for 2 sets")
println("Week 13 - \((Int)(1.04 * (Float)(oneRepMax)))x1 for 1 set")
println("Week 14 - \((Int)(1.07 * (Float)(oneRepMax)))x1 for 1 set")
println("Expected One Rep Max: \((Int)((Float)(oneRepMax) * 1.04)), \((Int)((Float)(oneRepMax) * 1.07)),  \((Int)((Float)(oneRepMax) * 1.11))")
 
